var oldInput = document.getElementById("oldInput"),
    newInput = document.getElementById("newInput"),
    output = document.getElementById("output"),
    oldInputText = "",
    newInputText = "";

oldInput.addEventListener("input", function(){
    oldInputText = oldInput.value;
    nerf(oldInputText, newInputText);
    //console.log(oldInputText);
}, false);

newInput.addEventListener("input", function(){
    newInputText = newInput.value;
    nerf(oldInputText, newInputText);
    //console.log(newInputText);
}, false);

function nerf(oldInputText, newInputText){
    var oldInputWords = oldInputText.split('\n'),
        newInputWords = newInputText.split('\n'),
        oldInputMap = new Map(),
        newInputMap = new Map();

    for(let i in oldInputWords){
        oldInputMap.set(oldInputWords[i].split(' : ')[0],oldInputWords[i].split(' : ')[1]);
    }
    for(let i in newInputWords){
        newInputMap.set(newInputWords[i].split(' : ')[0],newInputWords[i].split(' : ')[1]);
    }
    //console.log(oldInputMap);
    //console.log(newInputMap);

    output.innerText = "";
    for(let key of newInputMap.keys()){
        if(oldInputMap.has(key)){
            var diff = newInputMap.get(key) - oldInputMap.get(key);
            if(diff > 0){
                output.innerText += key + " : " + diff + "\n";
            }
        }else{
            output.innerText += key + " : " + newInputMap.get(key) + "\n";
        }
    }
}